import { ComunicationService } from './../../services/comunication.service';
import { StarWarsService } from './../../services/star-wars.service';
import { Component, OnInit, Input } from '@angular/core';
import { HttpParams } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-especies',
  templateUrl: './especies.component.html',
  styleUrls: ['./especies.component.css']
})
export class EspeciesComponent implements OnInit {

  @Input() url : String;
  species:Object;
  page:string='1';

  constructor(protected starWarsService:StarWarsService,
    private route:ActivatedRoute,
    public comunicationService:ComunicationService) { }

    ngOnInit() {
         
  
      this.route.paramMap.subscribe(params=>{
          if(params.has('page')){
            console.log('page'+params.get('page'));
            this.page = params.get('page');
          }
          this.starWarsService.getPages('species',this.page)
          .subscribe(
            (data)=>{
              this.species=data;
              console.log(data);
            },
            (error)=>{
              console.error(error);
            }
          )
        }
      )   
  
      this.comunicationService.search=false;
      
  }

}