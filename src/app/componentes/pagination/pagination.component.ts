import { StarWarsService } from './../../services/star-wars.service';
import { ComunicationService } from './../../services/comunication.service';
import { Routes, Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Route } from '@angular/compiler/src/core';

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.css']
})
export class PaginationComponent implements OnInit {

  @Input() total : number;
  @Input() next : string;
  @Input() previous : string;
  @Input() forPage : number;
  @Input() page : number;

  @Output() pageActually = new EventEmitter<string>();  

  pages : any[] = []
  
  constructor(private router:Router,
              private comunicationService:ComunicationService,
              private starWarsService:StarWarsService,
              private route:ActivatedRoute,) { }

  ngOnInit() {
    console.log(this.total);
    console.log(this.forPage);

    for(let i=1;i<=Math.ceil(this.total/this.forPage);i++){
      this.pages[i-1]=i;
    }
    
  }

  goPage(element,page){
    this.pageActually.emit(page);
  }

  goPrevious(category){
    if(this.previous != null){
      let params = this.previous.split('=');
      this.router.navigate([category,'page',params[params.length-1]])
    }
  }

  goNext(category){
    if(this.next != null){
      let params = this.next.split('=');
      this.router.navigate([category,'page',params[params.length-1]])
    }
  }

  pageActive(page):boolean{
    if(this.page==page){
      return true;
    }else{
      return false;
    }
    
  }

}
