import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-details-extend',
  templateUrl: './details-extend.component.html',
  styleUrls: ['./details-extend.component.css']
})
export class DetailsExtendComponent implements OnInit {

  @Input() detail : any[]=[];
  @Input() title : string;
  
  isCollapsed = true;

  constructor() { }

  ngOnInit() {
    
  }

}
