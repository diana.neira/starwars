import { ComunicationService } from './../../services/comunication.service';
import { StarWarsService } from './../../services/star-wars.service';
import { Component, OnInit, Input } from '@angular/core';
import { HttpParams } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-personajes',
  templateUrl: './personajes.component.html',
  styleUrls: ['./personajes.component.css']
})
export class PersonajesComponent implements OnInit {

  @Input() url : String;
  people:Object;
  page:string='1';

  constructor(protected starWarsService:StarWarsService,
    private route:ActivatedRoute,
    public comunicationService:ComunicationService) { }

    ngOnInit() {
         
  
      this.route.paramMap.subscribe(params=>{
          if(params.has('page')){
            console.log('page'+params.get('page'));
            this.page = params.get('page');
          }
          this.starWarsService.getPages('people',this.page)
          .subscribe(
            (data)=>{
              this.people=data;
              console.log(data);
            },
            (error)=>{
              console.error(error);
            }
          )
        }
      )   
  
      this.comunicationService.search=false;
      
  }

}
