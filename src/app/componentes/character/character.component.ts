import { StarWarsService } from './../../services/star-wars.service';
import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-character',
  templateUrl: './character.component.html',
  styleUrls: ['./character.component.css']
})
export class CharacterComponent implements OnInit {

  @Input() url : String;
  character:Object;

  constructor(private starWars:StarWarsService,
              private route:ActivatedRoute) { }

  ngOnInit() {
    this.route.paramMap.subscribe(params=>{
      this.starWars.getApiUrl(this.url)
      .subscribe(
        (data)=>{
          this.character=data;
        },
        (error)=>{
          console.error(error);
        }
      )
    }
    )
  }

}
