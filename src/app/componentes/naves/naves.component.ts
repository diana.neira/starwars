import { ComunicationService } from './../../services/comunication.service';
import { StarWarsService } from './../../services/star-wars.service';
import { Component, OnInit, Input } from '@angular/core';
import { HttpParams } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-naves',
  templateUrl: './naves.component.html',
  styleUrls: ['./naves.component.css']
})
export class NavesComponent implements OnInit {

  @Input() url : String;
  starships:Object;
  page:string='1';

  constructor(protected starWarsService:StarWarsService,
    private route:ActivatedRoute,
    public comunicationService:ComunicationService) { }

    ngOnInit() {
         
  
      this.route.paramMap.subscribe(params=>{
          if(params.has('page')){
            console.log('page'+params.get('page'));
            this.page = params.get('page');
          }
          this.starWarsService.getPages('starships',this.page)
          .subscribe(
            (data)=>{
              this.starships=data;
              console.log(data);
            },
            (error)=>{
              console.error(error);
            }
          )
        }
      )   
  
      this.comunicationService.search=false;
      
  }

}
