import { ComunicationService } from './../../services/comunication.service';
import { StarWarsService } from './../../services/star-wars.service';
import { Component, OnInit } from '@angular/core';
import { HttpParams } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-planets',
  templateUrl: './planets.component.html',
  styleUrls: ['./planets.component.css']
})
export class PlanetsComponent implements OnInit {

  planets : object;
  page:string='1';

  constructor(protected starWarsService:StarWarsService,
              private route:ActivatedRoute,
              public comunicationService:ComunicationService) { }

  ngOnInit() {
    //const url = window.location.href;
    //let page;
    //let view = 'planets';    

    this.route.paramMap.subscribe(params=>{
        if(params.has('page')){
          console.log('page'+params.get('page'));
          this.page = params.get('page');
        }
        this.starWarsService.getPages('planets',this.page)
        .subscribe(
          (data)=>{
            this.planets=data;
            console.log(data);
          },
          (error)=>{
            console.error(error);
          }
        )
      }
    )   

    this.comunicationService.search=false;
    
  }

}
