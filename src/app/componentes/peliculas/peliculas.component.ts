import { ComunicationService } from './../../services/comunication.service';
import { StarWarsService } from './../../services/star-wars.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-peliculas',
  templateUrl: './peliculas.component.html',
  styleUrls: ['./peliculas.component.css']
})
export class PeliculasComponent implements OnInit {
  
  title = 'Star Wars';

  films : any[] = [];

  constructor(protected starWarsService:StarWarsService,
              private router:Router,
              public comunicationService:ComunicationService) { }

  ngOnInit() {
    this.starWarsService.getElement('films')
    .subscribe(
      (data)=>{
        this.comunicationService.films=data['results'];
      },
      (error)=>{
        console.error(error);
      }
    ) 

    this.comunicationService.search=true;
  }

  viewDetails(url:string){
    let array = url.split('/');
    let id = array[array.length-2];
    console.log(id);
    this.router.navigate(["/films",id]);
  }

}
