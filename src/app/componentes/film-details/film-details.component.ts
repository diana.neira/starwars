import { StarWarsService } from './../../services/star-wars.service';
import { Component, OnInit, NgModule } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-film-details',
  templateUrl: './film-details.component.html',
  styleUrls: ['./film-details.component.css']
})

export class FilmDetailsComponent implements OnInit {

  film : object;
  titles:object;

  constructor(protected starWarsService:StarWarsService,
              private route:ActivatedRoute) { }

  ngOnInit() {
    this.titles={ 'characters':'Characters',
                  'planets':'Planets'
                };
    this.route.paramMap.subscribe(params=>{
        if(params.has('id')){
          this.starWarsService.getFilm(params.get('id'))
          .subscribe(
            (data)=>{
              this.film=data;
              console.log(this.film);
            },
            (error)=>{
              console.error(error);
            }
          )
        }
      }
    )
    
  }

}
