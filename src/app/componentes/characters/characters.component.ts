import { StarWarsService } from './../../services/star-wars.service';
import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-characters',
  templateUrl: './characters.component.html',
  styleUrls: ['./characters.component.css']
})
export class CharactersComponent implements OnInit {

  @Input() url : string;
  @Input() name : string;

  planet:Object;

  constructor(private starWars:StarWarsService,
              private route:ActivatedRoute) { }

  ngOnInit() {
    if(this.url){
      this.route.paramMap.subscribe(params=>{
        this.starWars.getApiUrl(this.url)
          .subscribe(
            (data)=>{
              this.planet=data;
            },
            (error)=>{
              console.error(error);
            }
          )
        }
      )
    }
    else{
      this.planet={
        "name":this.name
      }
    }
  }
}