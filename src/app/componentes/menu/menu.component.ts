import { Router } from '@angular/router';
import { StarWarsService } from './../../services/star-wars.service';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Menu } from 'src/app/interfaces/menu';


import { ComunicationService } from './../../services/comunication.service';
import { DataService } from './../../services/data.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {
  
  menu: Observable<Menu[]>;
  filters : String;

  constructor( private dataService:DataService,
               public comunicationService:ComunicationService,
               private starWarsService:StarWarsService,
               private router:Router) { }

  ngOnInit() {
    this.menu = this.dataService.getMenu();
  }

  filter(){
    if (this.filters != ''){
      this.starWarsService.getFilmSearch(this.filters)
      .subscribe(
        (data)=>{
          this.comunicationService.films=data['results'];
        },
        (error)=>{
          console.error(error);
        }
      )
    }else{
      this.starWarsService.getElement('films')
      .subscribe(
        (data)=>{
          this.comunicationService.films=data['results'];
        },
        (error)=>{
          console.error(error);
        }
      )
    }
  }

  menuActive(component):boolean{
    let url = this.router.url.split('/');
    let componentActually = '/'+url[1];
    if(component==componentActually){
      return true;
    }
    else{
      return false;
    }
  }

}
