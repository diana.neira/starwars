import { StarWarsService } from './../../services/star-wars.service';
import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-personaje',
  templateUrl: './personaje.component.html',
  styleUrls: ['./personaje.component.css']
})
export class PersonajeComponent implements OnInit {

  @Input() url : string;
  @Input() name : string;

  planet:Object;

  constructor(private starWars:StarWarsService,
              private route:ActivatedRoute) { }

  ngOnInit() {
    if(this.url){
      this.route.paramMap.subscribe(params=>{
        this.starWars.getApiUrl(this.url)
          .subscribe(
            (data)=>{
              this.planet=data;
            },
            (error)=>{
              console.error(error);
            }
          )
        }
      )
    }
    else{
      this.planet={
        "name":this.name
      }
    }
  }
}
