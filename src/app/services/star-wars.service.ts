import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class StarWarsService {

  api = "https://swapi.py4e.com/api/";

  constructor(private http:HttpClient) { }

  getApi(){
    return this.http.get(this.api);
  }

  getElement(id){
    return this.http.get(this.api+id);
  }

  getCharacter(id){
    return this.http.get(this.api+'people/'+id);
  }

  getFilm(id){
    return this.http.get(this.api+'films/'+id);
  }

  getFilmSearch(search){
    return this.http.get(this.api+'films/?search='+search);
  }

  getPages(category:string,page:string){
    return this.http.get(this.api+category+'/?page='+page);
  }

  getApiUrl(url:String){
    return this.http.get(url+'');
  }
}
