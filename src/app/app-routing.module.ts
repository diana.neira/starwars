import { FilmDetailsComponent } from './componentes/film-details/film-details.component';
import { Page404Component } from './componentes/page404/page404.component';
import { PlanetsComponent } from './componentes/planets/planets.component';
import { PeliculasComponent } from './componentes/peliculas/peliculas.component';
import { PersonajesComponent } from './componentes/personajes/personajes.component';
import { NavesComponent } from './componentes/naves/naves.component';
import { EspeciesComponent } from './componentes/especies/especies.component';
import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

const routes:Routes = [
  {path:'',component:PeliculasComponent},  
  {path:'films',component:PeliculasComponent},  
  {path:'films/:id',component:FilmDetailsComponent},
  {path:'planets',component:PlanetsComponent},
  {path:'planets/page/:page',component:PlanetsComponent}, 
  {path:'characters',component:PersonajesComponent},
  {path:'characters/page/:page',component:PersonajesComponent}, 
  {path:'ships',component:NavesComponent},
  {path:'ships/page/:page',component:NavesComponent}, 
  {path:'species',component:EspeciesComponent},
  {path:'species/page/:page',component:EspeciesComponent}, 
  {path:'**',component:Page404Component}
]

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(routes),    
  ],
  exports: [
    RouterModule
  ],
  declarations: []
})
export class AppRoutingModule { }
