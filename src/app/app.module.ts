import { RouterModule, Routes } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule} from '@angular/common/http';
import { CollapseModule } from 'ngx-bootstrap/collapse';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {FormsModule} from '@angular/forms';

import { AppComponent } from './app.component';
import { MenuComponent } from './componentes/menu/menu.component';
import { PeliculasComponent } from './componentes/peliculas/peliculas.component';
import { PlanetsComponent } from './componentes/planets/planets.component';
import { Page404Component } from './componentes/page404/page404.component';
import { AppRoutingModule } from './app-routing.module';
import { FilmDetailsComponent } from './componentes/film-details/film-details.component';
import { CharacterComponent } from './componentes/character/character.component';
import { DetailsExtendComponent } from './componentes/details-extend/details-extend.component';
import { PlanetComponent } from './componentes/planet/planet.component';
import { PaginationComponent } from './componentes/pagination/pagination.component';
import { CharactersComponent } from './componentes/characters/characters.component';
import { PersonajeComponent } from './componentes/personaje/personaje.component';
import { PersonajesComponent } from './componentes/personajes/personajes.component';
import { NaveComponent } from './componentes/nave/nave.component';
import { NavesComponent } from './componentes/naves/naves.component';
import { EspecieComponent } from './componentes/especie/especie.component';
import { EspeciesComponent } from './componentes/especies/especies.component';

@NgModule({
  declarations: [
    AppComponent,
    MenuComponent,
    PeliculasComponent,
    PlanetsComponent,
    Page404Component,
    FilmDetailsComponent,
    CharacterComponent,
    DetailsExtendComponent,
    PlanetComponent,
    PaginationComponent,
    CharactersComponent,
    PersonajeComponent,
    PersonajesComponent,
    NaveComponent,
    NavesComponent,
    EspecieComponent,
    EspeciesComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    CollapseModule, 
    BrowserAnimationsModule,
    FormsModule
  ],
  providers: [
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
